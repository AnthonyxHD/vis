import dash
from dash import dcc, html
import plotly.express as px
import pandas as pd

df = pd.read_csv('herkunft.csv', delimiter=';')

default_selection = df['category'].head(2).tolist()

app = dash.Dash(__name__)

app.layout = html.Div([
    dcc.Checklist(
        id='category-checklist',
        options=[{'label': category, 'value': category} for category in df['category']],
        value=default_selection
    ),
    dcc.Graph(id='pie-chart')
])

@app.callback(
    dash.dependencies.Output('pie-chart', 'figure'),
    [dash.dependencies.Input('category-checklist', 'value')]
)
def update_pie_chart(selected_categories):
    filtered_data = df[df['category'].isin(selected_categories)]
    
    
    color_scale = px.colors.sequential.Blues_r  
    
    fig = px.pie(filtered_data, names='category', values='quantity', hole=0.5, color_discrete_sequence=color_scale)
    
    fig.update_traces(textposition='outside', textinfo='percent+label', hovertemplate='n=%{value}')

    
    return fig

if __name__ == '__main__':
    app.run_server(debug=True)