#!/usr/bin/env python
# coding: utf-8

# In[1]:


from dash import Dash, html, dcc, Output, Input
import plotly.express as px
import pandas as pd
from pandas.api.types import CategoricalDtype
from dash_iconify import DashIconify

header_list = ["Alter", "Geschlecht", "gedruckte Bücher", "Audiobooks", "E-Books", "Nachrichten/Zeitung", "Magazine/Journals"]
df = pd.read_csv("data_v2.csv", names=header_list, skiprows=1)

# Reihenfolge der Altersgruppen
age_order = ['16-24', '25-34', '35-44', '45-54', '55-64', '65-74', '75-84', '85-93']

df['Alter'] = df['Alter'].astype(CategoricalDtype(categories=age_order, ordered=True))

app = Dash(__name__)

app.layout = html.Div([
    html.H4(id='medium-title'),
    html.Div([
        dcc.RadioItems(
            id='medium-radio',
            options=[
                {'label': DashIconify(
                    icon='bi:book', width=30
                ),
                 'value': 'gedruckte Bücher',
                },
                
                {'label': DashIconify(
                    icon='arcticons:audiobookshelf', width=40
                ),
                 'value': 'Audiobooks',
                },
                 
                {'label': DashIconify(
                    icon='flat-color-icons:kindle', width=30
                ),
                 'value': 'E-Books',
                },
                    
                {'label': DashIconify(
                    icon='emojione-monotone:newspaper', width=30
                ),
                 'value': 'Nachrichten/Zeitung',
                },
                 
                {'label': DashIconify(
                    icon='game-icons:newspaper', width=30
                ),
                 'value': 'Magazine/Journals',
                },
            ],
            value='Audiobooks',
            labelStyle={'display': 'inline-block'}
        ),
    ]),
    dcc.Graph(id='histogram-chart')
])

@app.callback(
    [Output('histogram-chart', 'figure'),
     Output('medium-title', 'children')],
    [Input('medium-radio', 'value')]
)
def update_histogram(selected_medium):
    # Auswahl der relevanten Spalten (Alter, Geschlecht und ausgewähltes Medium)
    selected_columns = ['Alter', 'Geschlecht', selected_medium]
    filtered_df = df[selected_columns]

    # Filtern der Daten, sodass nur "Yes" in den ausgewählten Medien angezeigt werden
    filtered_df = filtered_df[filtered_df[selected_medium] == 'Yes']
    filtered_df_melted = pd.melt(filtered_df, id_vars=['Alter', 'Geschlecht'], var_name='Medium', value_name='Anzahl')

    fig = px.histogram(filtered_df_melted, x="Alter", color="Geschlecht", barmode='group', category_orders={"Alter": age_order})

    # Titel
    medium_title = f'Mediumtyp: {selected_medium}'

    # Y-Achse 
    fig.update_layout(yaxis_title='Anzahl')

    # Hover-Text 
    fig.update_traces(hovertemplate='Alter: %{x}<br>Anzahl: %{y}')

    # Legende auf Deutsch umbenennen
    fig.for_each_trace(lambda t: t.update(name='Frauen' if t.name == 'Female' else 'Männer' if t.name == 'Male' else t.name))

    # Grösse des Histogramms
    fig.update_layout(height=450, width=600)

    return fig, medium_title

if __name__ == '__main__':
    app.run_server(port=3225)


# In[ ]:




