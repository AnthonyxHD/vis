#!/usr/bin/env python
# coding: utf-8

# In[16]:


from dash import Dash, dcc, html, Input, Output
import plotly.express as px
import pandas as pd 

header_list = ["Age", "Sex", "Books read"]
df = pd.read_csv("neu_auf.csv", names=header_list, skiprows=[0])

app = Dash(__name__)

app.layout = html.Div([
    html.H4('Number of books read in the last 12 months'),
    dcc.Graph(id="graph"),
    html.P("Sex:"),
    dcc.Checklist(
        id='sex1',
        options=["Female", "Male"],
        value=["Female"],
    ),
])

@app.callback(
    Output("graph", "figure"),
    Input("sex1", "value"))
def filter_heatmap(sex_values):  
    filtered_df = df[df['Sex'].isin(sex_values)]
    fig = px.density_heatmap(filtered_df, x="Books read", y="Age")
    fig.update_coloraxes(colorbar_title_text="Number of books")
    return fig

if __name__ == '__main__':
    app.run_server(port=2228)


# In[ ]:




