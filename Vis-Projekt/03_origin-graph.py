import dash
from dash import dcc, html
import plotly.express as px
import pandas as pd

df = pd.read_csv('dataset/herkunft-medien.csv', delimiter=';') # Daten einlesen

all_categories = df['category'].unique()  # Kategorien
default_selection = all_categories.tolist()  # Alle Kategorien als 'default' auswählen

app = dash.Dash(__name__) # Dash-App erstellen

# Layout definieren
app.layout = html.Div([
    dcc.Store(id='selected-categories', data=default_selection),  # Store selected categories

    html.Div([
        html.Div([
            dcc.Graph(id='pie-chart')
        ], style={'width': '60%', 'display': 'inline-block', 'vertical-align': 'middle'}),

        html.Div([
            html.Div(
                id='category-buttons',
                children=[
                    html.Button(category, id={'type': 'category-button', 'index': i}, n_clicks=0, className='button-origin button-origin-%d' % i) for i, category in enumerate(all_categories)
                ]
            ),
        ], style={'width': '30%', 'display': 'inline-block'}),
    ])
])

### Callbacks erstellen
# Update der ausgewählten Kategorien
@app.callback(
    [dash.dependencies.Output('selected-categories', 'data')],
    [dash.dependencies.Input({'type': 'category-button', 'index': dash.dependencies.ALL}, 'n_clicks')],
    [dash.dependencies.State('selected-categories', 'data')]
)
def update_selected_categories(n_clicks, current_selection):
    triggered_button_id = dash.callback_context.triggered_id
    if triggered_button_id is not None:
        index = triggered_button_id['index']
        current_selection[index] = not current_selection[index]  # Toggle the selected category

    return [current_selection]

#Aktualisierung des Tortendiagramms
@app.callback(
    dash.dependencies.Output('pie-chart', 'figure'),
    [dash.dependencies.Input('selected-categories', 'data')]
)
def update_pie_chart(selected_categories):
    filtered_data = df[df['category'].isin([category for i, category in enumerate(all_categories) if selected_categories[i]])]

    color_scale = px.colors.sequential.Blues_r
    fig = px.pie(
        filtered_data, 
        names='category', 
        values='quantity', 
        hole=0.5, 
        color_discrete_sequence=color_scale,
        title='Herrkunft der Medien')
    fig.update_traces(textposition='outside', textinfo='percent+label', hovertemplate='n=%{value}')
    fig.update_layout(
        plot_bgcolor='rgb(230, 230, 230)',
        paper_bgcolor='rgb(232, 223, 216)',
        font=dict(
            color='rgb(28, 28, 28)',
            family='Arial, sans serif',
            size=12
        )
    ) 

    return fig

# Aufwendung starten auf dritten Port
if __name__ == '__main__':
    app.run_server(debug=True, port=8052)