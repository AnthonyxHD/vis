from dash import Dash, dcc, html, Input, Output, callback
import plotly.express as px
import pandas as pd

### Daten einlesen
file_path = 'C:/Users/Antho/OneDrive/Desktop/Reading habit Dataset.csv'
reading_habits_data = pd.read_csv(file_path, sep=';')

### Daten bereinigen und vorbereiten
reading_habits_data = reading_habits_data[['Age', 'Sex', 'Race', 'Marital status?', 'Education', 'Employement', 'Incomes', 'How many books did you read during last 12months?']]
reading_habits_data.dropna(inplace=True)
reading_habits_data['How many books did you read during last 12months?'] = pd.to_numeric(
    reading_habits_data['How many books did you read during last 12months?'], errors='coerce')
reading_habits_data.dropna(subset=['How many books did you read during last 12months?'], inplace=True)

### Korrektur des Tippfehlers in den CSV-Rohdaten von "9$100" in "$100".
reading_habits_data = reading_habits_data.replace('9$100,000 to under $150,000', '$100,000 to under $150,000')

### Dash-App initialisieren
app = Dash(__name__, external_stylesheets=['VIS_main.css'])

### Layout-Einstellungen
# Initialer Stil für die Buttons Mann und Frau
default_style = {
    'backgroundColor': 'white',
    'color': 'black',
    'border': 'none',
    'padding': '10px 20px',
    'margin': '5px',

    'cursor': 'pointer'
}
# Stil für geklickte Buttons Mann und Frau
active_style = {
    'backgroundColor': '#CCCCCC', 
    'color': 'black',
    'border': 'none',
    'padding': '10px 20px',
    'margin': '5px',
    'cursor': 'pointer'
}

# Dropdown Menues
age_dropdown = dcc.Dropdown(
    id='age-dropdown',
    options=[
        {'label': 'Unter 20', 'value': 'under_20'},
        {'label': '20-40', 'value': '20_40'},
        {'label': 'Über 40', 'value': 'over_40'}
    ],
    style={'backgroundColor': 'white', 'color': 'black', 'width': '280px', 'margin-top' : '5px'},
    placeholder="Alter"
)
origin_dropdown = dcc.Dropdown(
    id='origin-dropdown',
    options=[
        {'label': 'Asian or Pacific Islander', 'value': 'Asian or Pacific Islander'},
        {'label': 'Black or African-American', 'value': 'Black or African-American'},
        {'label': 'Mixed race', 'value': 'Mixed race'},
        {'label': 'Native American/American Indian', 'value': 'Native American/American Indian'},
        {'label': 'White', 'value': 'White'},
        {'label': 'Other', 'value': 'Other'},
        {'label': 'Don’t know', 'value': 'Don’t know'},
        {'label': 'Refused', 'value': 'Refused'},
    ],
    style={'backgroundColor': 'white', 'color': 'black', 'width': '280px', 'margin-top' : '5px'},
    placeholder="Herkunft"
)
marital_status_dropdown = dcc.Dropdown(
    id='marital-status-dropdown',
    options=[
        {'label': 'Married', 'value': 'Married'},
        {'label': 'Living with a partner', 'value': 'Living with a partner'},
        {'label': 'Divorced', 'value': 'Divorced'},
        {'label': 'Separated', 'value': 'Separated'},
        {'label': 'Widowed', 'value': 'Widowed'},
        {'label': 'Single', 'value': 'Single'},
        {'label': 'Never been married', 'value': 'Never been married'},
        {'label': 'Don’t know', 'value': 'Don’t know'}
    ],
    style={'backgroundColor': 'white', 'color': 'black', 'width': '280px', 'margin-top' : '5px'},
    placeholder="Zivilstand"
)
education_dropdown = dcc.Dropdown(
    id='education-dropdown',
    options=[
        {'label': 'Post-graduate training/professional school after college', 'value': 'Post-graduate training/professional school after college'},
        {'label': 'College graduate', 'value': 'College graduate'},
        {'label': 'Some college, no 4-year degree', 'value': 'Some college, no 4-year degree'},
        {'label': 'Technical, trade or vocational school AFTER high school', 'value': 'Technical, trade or vocational school AFTER high school'},
        {'label': 'High school graduate', 'value': 'High school graduate'},
        {'label': 'High school incomplete', 'value': 'High school incomplete'},
        {'label': 'Don’t know', 'value': 'Don’t know'}
    ],
    style={'backgroundColor': 'white', 'color': 'black', 'width': '420px', 'margin-top' : '5px'},
    placeholder="Bildungsstand"
)
employment_dropdown = dcc.Dropdown(
    id='employment-dropdown',
    options=[
        {'label': 'Employed full-time', 'value': 'Employed full-time'},
        {'label': 'Employed part-time', 'value': 'Employed part-time'},
        {'label': 'Have own business/self-employed', 'value': 'Have own business/self-employed'},
        {'label': 'Student', 'value': 'Student'},
        {'label': 'Retired', 'value': 'Retired'},
        {'label': 'Disabled', 'value': 'Disabled'},
        {'label': 'Not employed for pay', 'value': 'Not employed for pay'},
        {'label': 'Other', 'value': 'Other'}
    ],
    style={'backgroundColor': 'white', 'color': 'black', 'width': '420px', 'margin-top' : '5px'},
    placeholder="Anstellung"
)
salary_dropdown = dcc.Dropdown(
    id='salary-dropdown',
    options=[
        {'label': 'Less than $10,000', 'value': 'Less than $10,000'},
        {'label': '$10,000 to under $20,000', 'value': '$10,000 to under $20,000'},
        {'label': '$20,000 to under $30,000', 'value': '$20,000 to under $30,000'},
        {'label': '$30,000 to under $40,000', 'value': '$30,000 to under $40,000'},
        {'label': '$40,000 to under $50,000', 'value': '$40,000 to under $50,000'},
        {'label': '$50,000 to under $75,000', 'value': '$50,000 to under $75,000'},
        {'label': '$75,000 to under $100,000', 'value': '$75,000 to under $100,000'},
        {'label': '$100,000 to under $150,000', 'value': '$100,000 to under $150,000'},
        {'label': 'Refused', 'value': 'Refused'}
    ],
    style={'backgroundColor': 'white', 'color': 'black', 'width': '420px', 'margin-top' : '5px'},
    placeholder="Einkommen"
)


### Layout der App definieren
app.layout = html.Div(className="container", children=[
    # Container für Diagramm
    html.Div(className="grafik_links_oben", children=[
        dcc.Graph(id='books-read-gender-graph'),
    # Container für Grundeinstellungen
    html.Div([
        # Container für die Buttons
        html.Div([
            # Button Mann -> Div damit Sie untereinander gezeigt werden
            html.Div([html.Button('Mann', id='toggle-male', n_clicks=0, style=default_style)]),
            # Button Frau
            html.Div([ html.Button('Frau', id='toggle-female', n_clicks=0, style=default_style)])
        ], style={'display': 'flex', 'flex-direction': 'column', 'margin-right': '50px'}),

        # Container für die Dropdowns
        html.Div([
            age_dropdown,
            origin_dropdown,
            marital_status_dropdown
        ], style={'display': 'flex', 'flex-direction': 'column', 'margin-right': '50px'}),
       
        html.Div([
            education_dropdown,
            employment_dropdown,
            salary_dropdown
        ], style={'display': 'flex', 'flex-direction': 'column'}),

    ], style={'display': 'flex', 'align-items': 'flex-start'})

    ])
])

### Callback definieren, um das Diagramm zu aktualisieren
@app.callback(
    Output('books-read-gender-graph', 'figure'),
    [
        Input('toggle-male', 'n_clicks'),
        Input('toggle-female', 'n_clicks'),
        Input('age-dropdown', 'value'),
        Input('origin-dropdown', 'value'),
        Input('marital-status-dropdown', 'value'),
        Input('education-dropdown', 'value'),
        Input('employment-dropdown', 'value'),
        Input('salary-dropdown', 'value'),
    ]
)
def update_graph(toggle_male_clicks, toggle_female_clicks, age_dropdown_value, origin_dropdown_value, marital_status_dropdown_value, education_dropdown_value, employment_dropdown_value, salary_dropdown_value):
    print("Updating graph")
    # Daten filtern basierend auf Sex Button-Klicks
    filtered_data = reading_habits_data.copy()
    if toggle_male_clicks % 2 != 0:
        print("Filter for male")
        filtered_data = filtered_data[filtered_data['Sex'] != 'Male']
    if toggle_female_clicks % 2 != 0:
        print("Filter for female")
        filtered_data = filtered_data[filtered_data['Sex'] != 'Female']

    # Daten filtern basierend auf age dropdown
    match age_dropdown_value:
        case "under_20":
            print("Filter for age under 20")
            filtered_data = filtered_data[filtered_data['Age'] < 20]

        case "20_40":
            print("Filter for age 20 - 40")
            filtered_data = filtered_data[(filtered_data['Age'] >= 20) & (filtered_data['Age'] <= 40)]

        case "over_40":
            print("Filter for age over 40")
            filtered_data = filtered_data[filtered_data['Age'] > 40]

    # Daten filtern basierend auf origin dropdown
    if origin_dropdown_value != None:
        print("Filter for origin: " + origin_dropdown_value)
        filtered_data = filtered_data[filtered_data['Race'] == origin_dropdown_value]

    # Daten filtern basierend auf marital status dropdown
    if marital_status_dropdown_value != None:
        print("Filter for marital status: " + marital_status_dropdown_value)
        filtered_data = filtered_data[filtered_data['Marital status?'] == marital_status_dropdown_value]

    # Daten filtern basierend auf education dropdown
    if education_dropdown_value != None:
        print("Filter for education: " + education_dropdown_value)
        filtered_data = filtered_data[filtered_data['Education'] == education_dropdown_value]

    # Daten filtern basierend auf employment dropdown
    if employment_dropdown_value != None:
        print("Filter for employment: " + employment_dropdown_value)
        filtered_data = filtered_data[filtered_data['Employement'] == employment_dropdown_value]

    # Daten filtern basierend auf salary dropdown
    if salary_dropdown_value != None:
        print("Filter for salary: " + salary_dropdown_value)
        filtered_data = filtered_data[filtered_data['Incomes'] == salary_dropdown_value]

    # Erstellen und Zurückgeben des Diagramms
    fig = px.histogram(
        filtered_data, 
        x='How many books did you read during last 12months?', 
        color='Sex', 
        barmode='group',
        histnorm='percent',
        title='Verteilung der Anzahl der gelesenen Bücher nach Geschlecht',
        labels={'How many books did you read during last 12months?': 'Anzahl gelesener Bücher', 'Sex': 'Geschlecht'},
        color_discrete_map={'Male': 'blue', 'Female': 'red'}
    )
    ## Aussehen des Diagramms
    fig.update_layout(
        plot_bgcolor='rgb(230, 230, 230)', # Hintergrundfarbe des Diagramms
        paper_bgcolor='rgb(232, 223, 216)', # Hintergrundfarbe des äusseren Diagrammbereichs
        font=dict(
            color='rgb(28, 28, 28)', #Farbe des Textes
            family='Arial, sans-serif', #Schriftfamilie
            size=12 #Schriftgrösse
        )
    )
    return fig

### Callback-Funktion, um die Buttonfarbe zu ändern
@app.callback(
    [Output('toggle-male', 'style'), Output('toggle-female', 'style')],
    [Input('toggle-male', 'n_clicks'), Input('toggle-female', 'n_clicks')]
)
def toggle_button_styles(n_clicks_male, n_clicks_female):
    # Wechsle den Stil des männlichen Buttons, wenn er geklickt wird
    style_male = active_style if n_clicks_male % 2 != 0 else default_style
    # Wechsle den Stil des weiblichen Buttons, wenn er geklickt wird, 
    style_female = active_style if n_clicks_female % 2 != 0 else default_style

    return style_male, style_female

### App ausführen
if __name__ == '__main__':
    app.run_server(debug=True)
